import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {ToastProvider} from 'react-native-toast-notifications';
import {NavigationContainer} from '@react-navigation/native';

import StartStack from './src/utils/StartStack';
import AuthContextComponent from './src/store/AuthContext';

const App = () => {
  return (
    <AuthContextComponent>
      <ToastProvider>
        <SafeAreaProvider>
          <NavigationContainer>
            <StartStack />
          </NavigationContainer>
        </SafeAreaProvider>
      </ToastProvider>
    </AuthContextComponent>
  );
};

export default App;
