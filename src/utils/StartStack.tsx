import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import About from '../screens/About/About';
import BuyProduct from '../screens/Buy/BuyProduct';
import Login from '../screens/Login/Login';
import CreateProduct from '../screens/Sell/CreateProduct';
import Signup from '../screens/SignUp/Signup';
import HomeTabs from './HomeTabs';

type Props = {};

export default function StartStack({}: Props) {
  const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="login" component={Login} />
      <Stack.Screen name="signup" component={Signup} />
      <Stack.Screen name="home" component={HomeTabs} />
      <Stack.Screen name="createproduct" component={CreateProduct} />
      <Stack.Screen name="BuyProduct" component={BuyProduct} />
      <Stack.Screen name="about" component={About} />
    </Stack.Navigator>
  );
}
