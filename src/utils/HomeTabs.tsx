import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import Buy from '../screens/Buy/Buy';
import Home from '../screens/Home/Home';
import Ionicons from 'react-native-vector-icons/Ionicons';
import appTheme from '../theme/appTheme';
import Sell from '../screens/Sell/Sell';
import Dashboard from '../screens/Dashboard/Dashboard';
import Profile from '../screens/Profile/Profile';

type Props = {};
const {colors} = appTheme;

export default function HomeTabs({}: Props) {
  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator
      initialRouteName="home"
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName = '';

          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home-outline';
          } else if (route.name === 'Comprar') {
            iconName = focused ? 'cart' : 'cart-outline';
          } else if (route.name === 'Vender') {
            iconName = focused ? 'pricetag' : 'pricetag-outline';
          } else if (route.name === 'Ingresos') {
            iconName = focused ? 'wallet' : 'wallet-outline';
          } else if (route.name === 'Perfil') {
            iconName = focused ? 'person' : 'person-outline';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: colors.main500,
        tabBarInactiveTintColor: 'gray',
        headerShown: false,
      })}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Comprar" component={Buy} />
      <Tab.Screen name="Vender" component={Sell} />
      <Tab.Screen name="Ingresos" component={Dashboard} />
      <Tab.Screen name="Perfil" component={Profile} />
    </Tab.Navigator>
  );
}
