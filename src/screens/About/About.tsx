import React from 'react';
import {StyleSheet, Text} from 'react-native';
import Divider from '../../components/Divider';
import Section from '../../components/Section';
import MainLayout from '../../layouts/MainLayout';
import appTheme from '../../theme/appTheme';

type Props = {};
const {colors} = appTheme;
export default function About({}: Props) {
  return (
    <MainLayout>
      <Section>
        <Text style={style.title}>Acerca de</Text>
        <Divider height={20} />
        <Text style={style.text}>Nombre del proyecto : Santorini Store</Text>
        <Divider height={10} />
        <Text style={style.text}>
          Datos de los integrantes del proyectos: Rey de la rosa 8-864-2045
        </Text>
        <Divider height={10} />
        <Text style={style.text}>
          En este curso hemos aprendido el proceso desde una idea hasta la
          planificacion - prototipado - diseño y desarrollo de dicha idea, como
          cada punto y proceso es importante para la correcta ejecución de la
          misma.
        </Text>
        <Divider height={10} />
        <Text style={style.text}>
          Aprendimos que es mejor equivocarnos en la planificacion o hasta el
          desarrollo ya que realizar cambios en el desarrollo toma mucho mas
          tiempo y por ende dinero ya que podemos terminar con elementos
          directos a la basura por la mala planificación
        </Text>
      </Section>
    </MainLayout>
  );
}

const style = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: colors.black,
  },
  text: {
    fontSize: 18,

    color: colors.black,
  },
});
