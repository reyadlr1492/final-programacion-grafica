import React, {useContext} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import Button from '../../components/Button';
import Divider from '../../components/Divider';
import Section from '../../components/Section';
import MainLayout from '../../layouts/MainLayout';
import {AuthContext} from '../../store/AuthContext';
import appTheme from '../../theme/appTheme';
import useTienda from './hooks/useTienda';

type Props = {
  route: any;
  navigation: any;
};
const {colors} = appTheme;
export default function BuyProduct({route}: Props) {
  const {product} = route.params;
  const authData = useContext(AuthContext);
  const {buyProduct} = useTienda();
  return (
    <MainLayout>
      <Section>
        <View style={style.imageContainer}>
          <Image style={style.image} source={{uri: product.image_link}} />
        </View>
        <Divider height={20} />
        <View>
          <Text style={style.nombre}>{product.nombre}</Text>
          <Divider height={16} />
          <Text style={style.normalText}>
            Precio:{' '}
            {new Intl.NumberFormat('en-HOS', {
              style: 'currency',
              currency: 'USD',
              minimumFractionDigits: 2,
            }).format(product.precio)}
          </Text>
          <Divider height={8} />
          <Text style={style.normalText}>Descripción:</Text>
          <Text style={style.normalText}>{product.descripcion}</Text>
          <Divider height={8} />
          <Text style={style.normalText}>Contacto: {product.telefono}</Text>
        </View>
        <View style={style.btnContainer}>
          <Button
            handler={() =>
              buyProduct(
                product.nombre,
                product.precio,
                product.image_link,
                product.user_id,
              )
            }>
            <Text style={style.btnText}>Comprar</Text>
          </Button>
        </View>
      </Section>
    </MainLayout>
  );
}

const style = StyleSheet.create({
  image: {
    width: 200,
    height: 200,
    borderRadius: 8,
  },
  imageContainer: {
    width: '100%',
    padding: 20,
    borderRadius: 8,
    alignItems: 'center',
    backgroundColor: colors.main100,
  },
  nombre: {
    fontSize: 24,
    fontWeight: 'bold',
    color: colors.black,
  },
  normalText: {
    fontSize: 18,
    color: colors.black,
  },
  btnContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 50,
  },
  btnText: {
    textAlign: 'center',
    fontSize: 18,
    color: colors.black,
  },
});
