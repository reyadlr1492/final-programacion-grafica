import React, {useState} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {useToast} from 'react-native-toast-notifications';
import {supabase} from '../../../utils/supabaseClient';
import ProductCard from '../../Sell/ProductCard';

type productValues = {
  userID: string;
  nombre: string;
  image_link: string;
  precio: number;
  telefono: string;
  descripcion: string;
};

type buyProductType = (
  nombre: string,
  precio: number,
  image_link: string,
  user_id: string,
) => void;

export default function useTienda() {
  const [isLoading, setIsLoading] = useState(false);
  const [storeList, setStoreList] = useState<
    React.ReactNode | Array<React.ReactNode> | []
  >([]);
  const toast = useToast();

  const handlerBuildStoreProducts = async (userID: string, navigation: any) => {
    setIsLoading(true);
    const {data: userProducts, error} = await supabase
      .from('Productos')
      .select('*')
      .filter('user_id', 'not.in', `(${userID})`);
    setIsLoading(false);
    if (error) {
      throw Error(error.message);
    } else {
      console.log(userProducts);
      const list = userProducts.map((product: productValues, i) => {
        return (
          <TouchableOpacity
            key={i}
            onPress={() =>
              navigation.navigate('BuyProduct', {
                product,
              })
            }
            style={style.btnStyle}>
            <ProductCard
              imageLink={product.image_link}
              nombre={product.nombre}
              precio={product.precio}
            />
          </TouchableOpacity>
        );
      });
      setStoreList(list);
    }
  };

  const buyProduct: buyProductType = async (
    nombre,
    precio,
    image_link,
    user_id,
  ) => {
    const {data: buyProduct, error} = await supabase
      .from('Ventas')
      .insert({nombre, precio, image_link, user_id});
    setIsLoading(false);
    if (error) {
      throw Error(error.message);
    } else {
      toast.show('Producto Comprado!', {duration: 3000, type: 'success'});
    }
  };
  return {isLoading, handlerBuildStoreProducts, storeList, buyProduct};
}

const style = StyleSheet.create({
  btnStyle: {
    width: '48%',
    marginBottom: 20,
  },
});
