import React, {useContext, useEffect} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import Divider from '../../components/Divider';
import Loading from '../../components/Loading';
import Section from '../../components/Section';
import MainLayout from '../../layouts/MainLayout';
import {AuthContext} from '../../store/AuthContext';
import appTheme from '../../theme/appTheme';
import NoProducts from '../Sell/NoProducts';
import useTienda from './hooks/useTienda';

const {colors} = appTheme;
type Props = {
  navigation: any;
};

export default function Buy({navigation}: Props) {
  const authData = useContext(AuthContext);
  const {storeList, isLoading, handlerBuildStoreProducts} = useTienda();
  useEffect(() => {
    handlerBuildStoreProducts(authData.user.id, navigation);
  }, []);
  return (
    <MainLayout>
      <Section>
        <View>
          <Text style={style.title}>Productos Disponibles</Text>
        </View>
        <Divider height={20} />
        {isLoading ? (
          <View style={style.loadingContainer}>
            <Loading />
          </View>
        ) : storeList.length < 1 ? (
          <View style={style.loadingContainer}>
            <NoProducts />
          </View>
        ) : (
          <ScrollView>
            <View style={style.productListContainer}>{storeList}</View>
          </ScrollView>
        )}
      </Section>
    </MainLayout>
  );
}

const style = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.black,
  },
  btnTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  productListContainer: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    height: '100%',
  },
});
