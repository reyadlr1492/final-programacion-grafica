import React, {useState} from 'react';
import {supabase} from '../../utils/supabaseClient';

export default function useDashboard() {
  const [totalVentas, setTotalVentas] = useState<null | number>(null);
  const [totalProductos, setTotalProductos] = useState<null | number>(null);

  const handlerGetData = async (userID: string) => {
    const getListProducts = await supabase
      .from('Productos')
      .select('*')
      .filter('user_id', 'in', `(${userID})`);
    const {data: listVentas} = await supabase
      .from('Ventas')
      .select('*')
      .filter('user_id', 'in', `(${userID})`);

    const total = listVentas?.reduce((prev, nextVal) => {
      return prev + nextVal.precio;
    }, 0);

    setTotalProductos(getListProducts.data?.length ?? 0);
    setTotalVentas(total);
  };
  return {handlerGetData, totalProductos, totalVentas};
}
