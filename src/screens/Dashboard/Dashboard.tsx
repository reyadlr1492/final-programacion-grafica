import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text} from 'react-native';
import Divider from '../../components/Divider';
import Section from '../../components/Section';
import MainLayout from '../../layouts/MainLayout';
import {AuthContext} from '../../store/AuthContext';
import appTheme from '../../theme/appTheme';
import useDashboard from './useDashboard';

type Props = {};
const {colors} = appTheme;
export default function Dashboard({}: Props) {
  const {handlerGetData, totalProductos, totalVentas} = useDashboard();
  const authData = useContext(AuthContext);
  useEffect(() => {
    handlerGetData(authData.user.id);
  }, []);
  return (
    <MainLayout>
      <Section>
        <Text style={style.title}>Cantidad productos en venta</Text>
        <Divider height={10} />
        <Text style={style.title}>Total: {totalProductos}</Text>
        <Divider height={30} />
        <Text style={style.title}>Total de dinero generado</Text>
        <Divider height={10} />
        <Text style={style.title}>
          Total:{' '}
          {new Intl.NumberFormat('en-HOS', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
          }).format(totalVentas ?? 0)}
        </Text>
      </Section>
    </MainLayout>
  );
}

const style = StyleSheet.create({
  title: {
    fontSize: 20,
    color: colors.black,
    fontWeight: 'bold',
  },
});
