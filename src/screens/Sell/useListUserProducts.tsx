import React, {useState} from 'react';
import {TouchableOpacity} from 'react-native';
import {useToast} from 'react-native-toast-notifications';
import {supabase} from '../../utils/supabaseClient';
import ProductCard from './ProductCard';

type createProduct = (
  userID: string,
  nombre: string,
  imagen: string,
  precio: number,
  telefono: string,
  descripcion: string,
) => void;

type productValues = {
  userID: string;
  nombre: string;
  imagen: string;
  precio: number;
  telefono: string;
  descripcion: string;
};
export default function useListUserProducts() {
  const toast = useToast();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [productList, setProductList] = useState<
    React.ReactNode | Array<React.ReactNode> | []
  >([]);

  const handlerBuildListProducts = async (userID: string, navigation: any) => {
    setIsLoading(true);
    const {data: userProducts, error} = await supabase
      .from('Productos')
      .select('*')
      .filter('user_id', 'in', `(${userID})`);
    setIsLoading(false);
    if (error) {
      console.log(error);
    } else {
      let isDivider = false;
      const list = userProducts.map((product: productValues, i) => {
        isDivider = !isDivider;
        return (
          <TouchableOpacity
            key={i}
            onPress={() =>
              navigation.navigate('createproduct', {
                product,
              })
            }
            style={{
              width: '48%',
              marginBottom: 20,
            }}>
            <ProductCard
              imageLink={product.image_link}
              nombre={product.nombre}
              precio={product.precio}
            />
          </TouchableOpacity>
        );
      });
      setProductList(list);
    }
  };

  const handlerCreateProduct: createProduct = async (
    userID,
    nombre,
    imagen,
    precio,
    telefono,
    descripcion,
  ) => {
    setIsLoading(true);
    const {data: userProducts, error} = await supabase
      .from('Productos')
      .insert({
        user_id: userID,
        nombre,
        image_link: imagen,
        precio,
        telefono,
        descripcion,
      })
      .filter('user_id', 'in', `(${userID})`);
    setIsLoading(false);
    if (error) {
      console.log(error);
      throw Error(error.message);
    } else {
      toast.show('Producto creado!', {duration: 3000, type: 'success'});
    }
  };

  const handlerUpdateProduct: createProduct = async (
    productID,
    nombre,
    imagen,
    precio,
    telefono,
    descripcion,
  ) => {
    setIsLoading(true);
    const {data: userProducts, error} = await supabase
      .from('Productos')
      .update({
        nombre,
        image_link: imagen,
        precio,
        telefono,
        descripcion,
      })
      .filter('id', 'in', `(${productID})`);
    setIsLoading(false);
    if (error) {
      console.log(error);
      throw Error(error.message);
    } else {
      toast.show('Producto Actualizado!', {duration: 3000, type: 'success'});
    }
  };

  const handlerDeleteProduct = async (productID: string) => {
    const {data: userProducts, error} = await supabase
      .from('Productos')
      .delete()
      .filter('id', 'in', `(${productID})`);
    setIsLoading(false);
    if (error) {
      throw Error(error.message);
    } else {
      toast.show('Producto Eliminado!', {duration: 3000, type: 'danger'});
    }
  };

  return {
    isLoading,
    handlerBuildListProducts,
    productList,
    handlerCreateProduct,
    handlerUpdateProduct,
    handlerDeleteProduct,
  };
}
