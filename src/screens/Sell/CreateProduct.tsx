import React, {useContext} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {StyleSheet, Text} from 'react-native';
import Button from '../../components/Button';
import Divider from '../../components/Divider';
import Field from '../../components/Field';
import Label from '../../components/Label';
import Section from '../../components/Section';
import MainLayout from '../../layouts/MainLayout';
import {AuthContext} from '../../store/AuthContext';
import appTheme from '../../theme/appTheme';
import useListUserProducts from './useListUserProducts';

type Props = {
  route: any;
  navigation: any;
};
const {colors} = appTheme;
export default function CreateProduct({route, navigation}: Props) {
  const product = route?.params?.product ?? false; // check if route have product data or not

  const {handlerCreateProduct, handlerUpdateProduct, handlerDeleteProduct} =
    useListUserProducts();
  const authData = useContext(AuthContext);
  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    defaultValues: {
      nombre: product ? product.nombre : '',
      descripcion: product ? product.descripcion : '',
      image_link: product ? product.image_link : '',
      precio: product ? product.precio.toString() : '',
      telefono: product ? product.telefono : '+507 ',
    },
  });

  const onSubmit = (data: {
    nombre: string;
    descripcion: string;
    image_link: string;
    precio: string;
    telefono: string;
  }) => {
    if (product) {
      try {
        handlerUpdateProduct(
          product.id,
          data.nombre,
          data.image_link,
          Number(data.precio),
          data.telefono,
          data.descripcion,
        );
        backPage();
      } catch (error) {}
      return;
    }
    try {
      handlerCreateProduct(
        authData.user.id,
        data.nombre,
        data.image_link,
        Number(data.precio),
        data.telefono,
        data.descripcion,
      );
      backPage();
    } catch (error) {}
  };

  const backPage = () => {
    setTimeout(() => navigation.navigate('Vender'), 2000);
  };

  return (
    <MainLayout>
      <Section>
        <Text style={style.title}>
          {product ? 'Actualizar producto' : 'Crear producto'}
        </Text>
        <Divider height={10} />
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <>
              <Label>Nombre</Label>
              <Field
                handlerBlur={onBlur}
                handleChange={onChange}
                value={value}
                passwordType={false}
              />
            </>
          )}
          name="nombre"
        />
        {errors.nombre && <Text>ingrese su nombre</Text>}

        <Divider height={20} />
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <>
              <Label>Imagen</Label>
              <Field
                handlerBlur={onBlur}
                handleChange={onChange}
                value={value}
              />
            </>
          )}
          name="image_link"
        />
        {errors.image_link && <Text>ingrese su imagen</Text>}
        <Divider height={20} />
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <>
              <Label>Precio</Label>
              <Field
                handlerBlur={onBlur}
                handleChange={onChange}
                value={value}
              />
            </>
          )}
          name="precio"
        />
        {errors.precio && <Text>ingrese su Precio</Text>}
        <Divider height={20} />
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <>
              <Label>Telefono</Label>
              <Field
                handlerBlur={onBlur}
                handleChange={onChange}
                value={value}
              />
            </>
          )}
          name="telefono"
        />
        {errors.telefono && <Text>ingrese su Telefono</Text>}
        <Divider height={20} />
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <>
              <Label>Descripción</Label>
              <Field
                handlerBlur={onBlur}
                handleChange={onChange}
                value={value}
                isTextArea
              />
            </>
          )}
          name="descripcion"
        />
        {errors.descripcion && <Text>ingrese su descripción</Text>}
        <Divider height={40} />
        <Button
          handler={() => {
            handleSubmit(onSubmit)();
          }}>
          <Text style={style.btnText}>
            {product ? 'Actualizar producto' : 'Añadir producto'}
          </Text>
        </Button>
        {product && (
          <>
            <Divider height={6} />
            <Button
              inline
              handler={() => {
                try {
                  handlerDeleteProduct(product.id);
                  backPage();
                } catch (error) {}
              }}>
              <Text style={style.btnDeleteText}>Eliminar producto</Text>
            </Button>
          </>
        )}
      </Section>
    </MainLayout>
  );
}

const style = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.black,
  },
  btnText: {
    fontSize: 16,
    textAlign: 'center',
  },
  btnDeleteText: {
    fontSize: 16,
    textAlign: 'center',
    color: 'red',
  },
});
