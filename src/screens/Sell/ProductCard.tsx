import React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';
import appTheme from '../../theme/appTheme';

type Props = {
  imageLink: string;
  precio: number;
  nombre: string;
};
const {colors} = appTheme;
export default function ProductCard({imageLink, precio, nombre}: Props) {
  return (
    <View style={style.container}>
      <View style={style.imageContainer}>
        <Image
          style={style.image}
          source={{
            uri: imageLink,
          }}
        />
      </View>
      <View style={style.contentContainer}>
        <Text style={style.nombre}>{nombre}</Text>
        <Text style={style.precio}>
          Precio:{' '}
          {new Intl.NumberFormat('en-HOS', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
          }).format(precio)}
        </Text>
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    width: '100%',
  },
  imageContainer: {
    alignItems: 'center',
    backgroundColor: colors.main200,
    borderRadius: 10,
    padding: 8,
  },
  image: {
    width: 120,
    height: 120,
    borderRadius: 10,
  },
  contentContainer: {
    padding: 8,
  },
  nombre: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.black,
  },
  precio: {
    fontSize: 16,
  },
});
