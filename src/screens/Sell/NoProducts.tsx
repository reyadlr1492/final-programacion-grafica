import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {SvgUri} from 'react-native-svg';
import appTheme from '../../theme/appTheme';

type Props = {};
const {colors} = appTheme;
export default function NoProducts({}: Props) {
  return (
    <View style={style.container}>
      <SvgUri
        uri={
          'https://res.cloudinary.com/dmvzdtvgt/image/upload/v1674156824/Santorini/noProducts_hpdrzv_szj5fm_zenl3a.svg'
        }
      />
      <Text style={style.title}> Aún no tienes productos en venta</Text>
      <Text style={style.text}>Empieza añadiendo un nuevo producto </Text>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    color: colors.black,
    fontWeight: 'bold',
    marginTop: 10,
  },
  text: {fontSize: 16, textAlign: 'center', color: colors.black, marginTop: 5},
});
