import {useIsFocused} from '@react-navigation/native';
import React, {useContext, useEffect} from 'react';
import {Button, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Divider from '../../components/Divider';
import Loading from '../../components/Loading';
import Section from '../../components/Section';
import MainLayout from '../../layouts/MainLayout';
import {AuthContext} from '../../store/AuthContext';
import appTheme from '../../theme/appTheme';
import NoProducts from './NoProducts';
import useListUserProducts from './useListUserProducts';

type Props = {
  navigation: any;
};
const {colors} = appTheme;
export default function Sell({navigation}: Props) {
  const {handlerBuildListProducts, isLoading, productList} =
    useListUserProducts();
  const authData = useContext(AuthContext);
  const isFocused = useIsFocused(); // help to trigger when back again to the page

  useEffect(() => {
    if (isFocused) {
      handlerBuildListProducts(authData.user.id, navigation);
    }
  }, [isFocused]);
  return (
    <MainLayout>
      <Section>
        <View style={style.btnTitleContainer}>
          <Text style={style.title}>Productos en Venta</Text>
          <TouchableOpacity
            onPress={() => {
              console.log('click')
              navigation.navigate('createproduct')
            }}

            style={style.btnNewProduct}>
            <Text>+ Nuevo producto</Text>
          </TouchableOpacity>
        </View>
        <Divider height={20} />
        {isLoading ? (
          <View style={style.loadingContainer}>
            <Loading />
          </View>
        ) : productList.length < 1 ? (
          <View style={style.loadingContainer}>
            <NoProducts />
          </View>
        ) : (
          <ScrollView>
            <View style={style.productListContainer}>{productList}</View>
          </ScrollView>
        )}
      </Section>
    </MainLayout>
  );
}

const style = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.black,
  },
  btnNewProduct: {
    backgroundColor: colors.main400,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderRadius: 4,
  },
  btnTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  productListContainer: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    height: '100%',
  },
});
