import React, {useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Button from '../../components/Button';
import Divider from '../../components/Divider';
import Section from '../../components/Section';
import MainLayout from '../../layouts/MainLayout';
import {AuthContext} from '../../store/AuthContext';
import appTheme from '../../theme/appTheme';

type Props = {
  navigation: any;
};
const {colors} = appTheme;
export default function Profile({navigation}: Props) {
  const authData = useContext(AuthContext);
  return (
    <MainLayout>
      <Section>
        <Text style={style.title}>Datos de usuario:</Text>
        <Divider height={16} />
        <Text style={style.titleInfo}>Nombre:</Text>
        <Text style={style.text}>{authData.user.nombre}</Text>
        <Divider height={10} />
        <Text style={style.titleInfo}>Apellido:</Text>
        <Text style={style.text}>{authData.user.apellido}</Text>
        <Divider height={10} />
        <Text style={style.titleInfo}>Dirección:</Text>
        <Text style={style.text}>{authData.user.direccion}</Text>
        <Divider height={10} />
        <Text style={style.titleInfo}>Email:</Text>
        <Text style={style.text}>{authData.user.email}</Text>
        <View style={style.btnContainer}>
          <Button handler={() => navigation.navigate('login')}>
            <Text style={style.btnText}>Cerrar Sessión</Text>
          </Button>
        </View>
      </Section>
    </MainLayout>
  );
}

const style = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.black,
  },
  titleInfo: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.black,
  },
  text: {
    fontSize: 16,
    color: colors.black,
  },
  btnContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'stretch',
    paddingBottom: 40,
  },
  btnText: {textAlign: 'center', fontSize: 16, color: colors.black},
});
