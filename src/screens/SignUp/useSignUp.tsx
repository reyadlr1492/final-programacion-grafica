import {PostgrestError, PostgrestResponse} from '@supabase/supabase-js';
import {useEffect, useState} from 'react';
import {supabase} from '../../utils/supabaseClient';
import {useToast} from 'react-native-toast-notifications';
type createAccountTypes = {
  nombre: string;
  apellido: string;
  direccion: string;
  email: string;
  password: string;
};

export default function useSignUp() {
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState<PostgrestError | null>(null);
  const [data, setData] = useState<PostgrestResponse<any> | null | undefined[]>(
    null,
  );

  const toast = useToast();

  useEffect(() => {
    if (isError !== null) {
      toast.show(isError?.message, {type: 'danger', duration: 3000});
    }
  }, [isError, toast]); // when is error fire toast notification

  const createAccount = async (userData: createAccountTypes) => {
    setIsLoading(true);
    const {data: accountData, error} = await supabase
      .from('Usuario')
      .insert(userData);
    setIsLoading(false);
    if (error) {
      setIsError(error);
    } else {
      toast.show('Cuenta creada', {type: 'success', duration: 3000});
      setData(accountData);
    }
  }; // handle create account

  return {isLoading, isError, data, createAccount};
}
