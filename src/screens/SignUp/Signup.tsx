import React from 'react';
import {Controller, SubmitHandler, useForm} from 'react-hook-form';
import {StyleSheet, Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Button from '../../components/Button';
import Divider from '../../components/Divider';
import Field from '../../components/Field';
import Label from '../../components/Label';

import Section from '../../components/Section';
import EnterLayout from '../../layouts/EnterLayout';
import appTheme from '../../theme/appTheme';
import useSignUp from './useSignUp';

const {colors} = appTheme;
type Props = {
  navigation: any;
};
type createAccountTypes = {
  nombre: string;
  apellido: string;
  direccion: string;
  email: string;
  password: string;
};

export default function Signup({navigation}: Props) {
  const {createAccount} = useSignUp();
  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    defaultValues: {
      email: '',
      password: '',
      direccion: '',
      nombre: '',
      apellido: '',
    },
  });
  const onSubmit: SubmitHandler<createAccountTypes> = data => {
    createAccount(data);
  };

  return (
    <EnterLayout>
      <ScrollView>
        <Section>
          <Text style={style.title}>Crear Cuenta</Text>
          <View>
            <View style={style.twoFieldRow}>
              <View style={style.fieldContainer}>
                <Controller
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({field: {onChange, onBlur, value}}) => (
                    <>
                      <Label>Nombre</Label>
                      <Field
                        handleChange={onChange}
                        handlerBlur={onBlur}
                        value={value}
                      />
                    </>
                  )}
                  name="nombre"
                />
                {errors.nombre && <Text>ingrese su nombre</Text>}
              </View>
              <Divider width={20} />
              <View style={style.fieldContainer}>
                <Controller
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({field: {onChange, onBlur, value}}) => (
                    <>
                      <Label>Apellido</Label>
                      <Field
                        handleChange={onChange}
                        handlerBlur={onBlur}
                        value={value}
                      />
                    </>
                  )}
                  name="apellido"
                />
                {errors.apellido && <Text>ingrese su apellido</Text>}
              </View>
            </View>

            <Divider height={16} />

            <Divider height={16} />
            <Controller
              control={control}
              rules={{
                required: true,
              }}
              render={({field: {onChange, onBlur, value}}) => (
                <>
                  <Label>Dirección</Label>
                  <Field
                    handleChange={onChange}
                    handlerBlur={onBlur}
                    value={value}
                  />
                </>
              )}
              name="direccion"
            />
            {errors.direccion && <Text>ingrese su dirección</Text>}

            <Divider height={16} />
            <Controller
              control={control}
              rules={{
                required: true,
              }}
              render={({field: {onChange, onBlur, value}}) => (
                <>
                  <Label>Email</Label>
                  <Field
                    handleChange={onChange}
                    handlerBlur={onBlur}
                    value={value}
                  />
                </>
              )}
              name="email"
            />
            {errors.email && <Text>ingrese su email</Text>}

            <Divider height={16} />
            <Controller
              control={control}
              rules={{
                required: true,
              }}
              render={({field: {onChange, onBlur, value}}) => (
                <>
                  <Label>Contraseña</Label>
                  <Field
                    handleChange={onChange}
                    handlerBlur={onBlur}
                    value={value}
                    passwordType={true}
                  />
                </>
              )}
              name="password"
            />
            {errors.password && <Text>ingrese su contraseña</Text>}
            <Divider height={30} />
            <View>
              <Button
                handler={() => {
                  handleSubmit(onSubmit)();
                }}>
                <Text style={style.buttonText}>Crear cuenta</Text>
              </Button>
              <Divider height={12} />
              <Button inline handler={() => navigation.navigate('login')}>
                <Text style={style.buttonText}>Iniciar Sesión</Text>
              </Button>
            </View>
          </View>
        </Section>
      </ScrollView>
    </EnterLayout>
  );
}

const style = StyleSheet.create({
  title: {
    color: colors.black,
    textAlign: 'center',
    fontSize: 26,
    marginBottom: 30,
  },

  twoFieldRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  fieldContainer: {
    flex: 1,
  },
  buttonText: {
    color: colors.black,
    fontSize: 18,
    textAlign: 'center',
  },
});
