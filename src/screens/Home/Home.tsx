import React, {useContext} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import Divider from '../../components/Divider';
import Section from '../../components/Section';
import MainLayout from '../../layouts/MainLayout';
import {AuthContext} from '../../store/AuthContext';
import SectionCard from './SectionCard';

type Props = {
  navigation: any;
};

export default function Home({navigation}: Props) {
  const AuthData = useContext(AuthContext);
  console.log(AuthData);
  return (
    <MainLayout>
      <Section>
        <View style={style.sectionRow}>
          <SectionCard
            title="Comprar"
            handlerRoute={() => navigation.navigate('Comprar')}>
            <Image source={require('../../media/comprarIllustration.png')} />
          </SectionCard>
          <SectionCard
            title="Vender"
            handlerRoute={() => navigation.navigate('Vender')}>
            <Image source={require('../../media/sellIlustation.png')} />
          </SectionCard>
        </View>
        <Divider height={40} />
        <View style={style.sectionRow}>
          <SectionCard
            title="Ingresos"
            handlerRoute={() => navigation.navigate('Ingresos')}>
            <Image source={require('../../media/dashboardIlustration.png')} />
          </SectionCard>
          <SectionCard
            title="Acerca de"
            handlerRoute={() => navigation.navigate('about')}>
            <Image source={require('../../media/AboutIlustration.png')} />
          </SectionCard>
        </View>
      </Section>
    </MainLayout>
  );
}

const style = StyleSheet.create({
  sectionRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
