import React from 'react';
import {Image, Text, View, TouchableOpacity, StyleSheet} from 'react-native';

type Props = {
  handlerRoute: () => {};
  children: React.ReactNode;
  title: string;
};

export default function SectionCard({handlerRoute, title, children}: Props) {
  return (
    <View>
      <Text style={style.sectionTitle}>{title}</Text>
      <TouchableOpacity onPress={handlerRoute}>{children}</TouchableOpacity>
    </View>
  );
}

const style = StyleSheet.create({
  sectionTitle: {
    fontSize: 20,
    fontWeight: 'normal',
    marginBottom: 10,
  },
});
