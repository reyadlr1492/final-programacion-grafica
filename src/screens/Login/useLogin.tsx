import {PostgrestResponse} from '@supabase/supabase-js';
import {useContext, useState} from 'react';
import {useToast} from 'react-native-toast-notifications';
import {supabase} from '../../utils/supabaseClient';
import {AuthContext} from '../../store/AuthContext';
export default function useLogin() {
  const toast = useToast();
  const authContext = useContext(AuthContext);
  const [userData, setUserData] = useState<
    PostgrestResponse<any> | null | Array<any>
  >([]);

  const loginHandler = async (email: string, password: string) => {
    try {
      const {data: tryLogin, error} = await supabase
        .from('Usuario')
        .select('*')
        .filter('email', 'in', `(${email})`)
        .filter('password', 'in', `(${password})`);
      if (error || tryLogin.length < 1) {
        toast.show('Datos incorrectos!', {
          type: 'danger',
          duration: 3000,
        });
        return;
      }
      setUserData(tryLogin);
      authContext.handlerSetValues(tryLogin[0]);
    } catch (error: any) {
      console.log(error);
    }
  };
  return {loginHandler, userData};
}
