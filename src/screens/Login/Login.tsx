import React, {useEffect} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Button from '../../components/Button';
import Divider from '../../components/Divider';
import Field from '../../components/Field';
import Label from '../../components/Label';
import Section from '../../components/Section';
import EnterLayout from '../../layouts/EnterLayout';
import appTheme from '../../theme/appTheme';
import useLogin from './useLogin';

const {colors} = appTheme;

type Props = {
  navigation: any;
};

export default function Login({navigation}: Props) {
  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
  });
  const {userData, loginHandler} = useLogin();
  const onSubmit = (data: {email: string; password: string}) =>
    loginHandler(data.email, data.password);

  useEffect(() => {
    if (userData !== null) {
      navigation.navigate('home');
    }
  }, [userData, navigation]);

  return (
    <EnterLayout>
      <Text style={style.title}>Inicio de sesión</Text>

      <Section>
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <>
              <Label>Email</Label>
              <Field
                handlerBlur={onBlur}
                handleChange={onChange}
                value={value}
                passwordType={false}
              />
            </>
          )}
          name="email"
        />
        {errors.email && <Text>ingrese su email</Text>}

        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <>
              <Label>Contraseña</Label>
              <Field
                handlerBlur={onBlur}
                handleChange={onChange}
                passwordType={true}
                value={value}
              />
            </>
          )}
          name="password"
        />
        {errors.password && <Text>ingrese su contraseña</Text>}
        <Divider height={20} />

        <View style={style.buttonContainer}>
          <Button
            handler={() => {
              handleSubmit(onSubmit)();
            }}>
            <Text style={style.buttonText}>Iniciar Sesión</Text>
          </Button>
          <Divider height={12} />
          <Button handler={() => navigation.navigate('signup')} inline>
            <Text style={style.buttonText}>Crear cuenta</Text>
          </Button>
        </View>
      </Section>
    </EnterLayout>
  );
}

const style = StyleSheet.create({
  title: {
    fontSize: 26,
    fontWeight: '500',
    textAlign: 'center',
    marginBottom: 40,
  },

  buttonText: {
    textAlign: 'center',
    fontSize: 18,
    color: colors.black,
  },
  buttonContainer: {
    marginTop: 'auto',
  },
});
