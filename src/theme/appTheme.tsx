export default {
  colors: {
    main50: '#FFFDEA',
    main100: '#FFF8C5',
    main200: '#FFF185',
    main300: '#FFE346',
    main400: '#FFD21B',
    main500: '#FFB100',
    main600: '#E28700',
    main700: '#BB5E02',
    main800: '#984808',
    main900: '#7C3B0B',
    black: '#3C3838',
    white: '#fff',
  },
};
