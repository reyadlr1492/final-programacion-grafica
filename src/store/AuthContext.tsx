import React, {useState} from 'react';

interface userType {
  nombre: '';
  apellido: '';
  email: '';
  password: '';
  created_at: '';
  id: '';
  direccion: '';
}
type Props = {
  children: React.ReactNode;
};

const initialContext = {
  user: {
    nombre: '',
    apellido: '',
    email: '',
    password: '',
    created_at: '',
    id: '',
    direccion: '',
  },
  handlerSetValues: (user: userType) => {},
  handlerGetValues: () => {},
};

export const AuthContext = React.createContext(initialContext);

export default function AuthContextComponent({children}: Props) {
  const [userData, setUserData] = useState<userType>({
    nombre: '',
    apellido: '',
    email: '',
    password: '',
    created_at: '',
    id: '',
    direccion: '',
  });

  const handlerSetValues = (user: userType) => {
    setUserData(user);
  };

  const handlerGetValues = () => {
    return userData;
  };
  return (
    <AuthContext.Provider
      value={{user: userData, handlerSetValues, handlerGetValues}}>
      {children}
    </AuthContext.Provider>
  );
}
