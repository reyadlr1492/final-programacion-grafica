import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {SvgUri} from 'react-native-svg';
import appTheme from '../theme/appTheme';

type Props = {
  children: React.ReactNode;
};
const {colors} = appTheme;
export default function EnterLayout({children}: Props) {
  const insets = useSafeAreaInsets();
  return (
    <View
      style={{
        ...style.container,
        flex: 1,
        paddingBottom: insets.bottom,
        paddingLeft: insets.left,
        paddingRight: insets.right,
      }}>
      <View>
        <SvgUri
          uri={
            'https://res.cloudinary.com/dmvzdtvgt/image/upload/v1674156835/Santorini/waveInicio_yvk7hz_giklaa_vmjwje.svg'
          }
        />
      </View>

      <View style={style.inicioDecoration}>
        <SvgUri
          uri={
            'https://res.cloudinary.com/dmvzdtvgt/image/upload/v1674156835/Santorini/inicioIlustration_y6gmwm_zzk44b_qad6j5.svg'
          }
        />
      </View>

      <View style={style.logoContainer}>
        <Text style={{...style.logo, paddingTop: insets.top}}>
          <Text style={style.firstLetterLogo}>S</Text>antorini
        </Text>
      </View>

      <View style={style.content}>{children}</View>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  logo: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
    color: colors.white,

    // left: '50%',
    // transform: [{translateX: -50}],
  },
  logoContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  firstLetterLogo: {
    color: colors.main400,
  },
  inicioDecoration: {
    position: 'absolute',
    top: 75,
    left: '50%',
    transform: [{translateX: -50}],
  },
  content: {
    flex: 1,
    marginTop: 40,
  },
});
