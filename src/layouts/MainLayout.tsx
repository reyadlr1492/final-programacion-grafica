import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Button from '../components/Button';
import appTheme from '../theme/appTheme';

type Props = {
  children: React.ReactNode;
};
const {colors} = appTheme;
export default function MainLayout({children}: Props) {
  const insets = useSafeAreaInsets();

  return (
    <View
      style={{
        ...style.container,
        flex: 1,

        paddingLeft: insets.left,
        paddingRight: insets.right,
      }}>
      <View>
        <Text style={{...style.logo, paddingTop: insets.top}}>
          <Text style={style.firstLetterLogo}>S</Text>antorini
        </Text>
      </View>

      <View style={style.content}>{children}</View>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  logo: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
    color: colors.black,
  },

  firstLetterLogo: {
    color: colors.main400,
  },

  content: {
    flex: 1,
    marginTop: 40,
  },

  navBarContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    backgroundColor: colors.black,
    justifyContent: 'space-between',
    paddingTop: 8,
  },

  navBarText: {
    color: colors.main50,
  },
});
