import React from 'react';
import {StyleSheet} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import appTheme from '../theme/appTheme';

type Props = {
  handlerBlur: any;
  handleChange: any;
  value: string;
  passwordType?: boolean;
  isTextArea?: boolean;
};

const {colors} = appTheme;

export default function Field({
  handlerBlur,
  handleChange,
  value,
  passwordType = false,
  isTextArea = false,
}: Props) {
  if (isTextArea) {
    return (
      <TextInput
        style={style.textArea}
        onBlur={handlerBlur}
        onChangeText={handleChange}
        value={value}
        secureTextEntry={passwordType}
        multiline={true}
        numberOfLines={4}
      />
    );
  }
  return (
    <TextInput
      style={style.input}
      onBlur={handlerBlur}
      onChangeText={handleChange}
      value={value}
      secureTextEntry={passwordType}
    />
  );
}

const style = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderRadius: 5,
    height: 44,
    borderColor: colors.main500,
    paddingHorizontal: 16,
    fontSize: 18,
  },
  textArea: {
    height: 90,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: colors.main500,
    paddingHorizontal: 16,
    fontSize: 18,
  },
});
