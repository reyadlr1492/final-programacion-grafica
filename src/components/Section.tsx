import React from 'react';
import {StyleSheet, View} from 'react-native';

type Props = {
  children: React.ReactNode;
};

export default function Section({children}: Props) {
  return <View style={style.container}>{children}</View>;
}

const style = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    flex: 1,
  },
});
