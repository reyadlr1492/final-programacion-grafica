import React from 'react';
import {View} from 'react-native';

type Props = {
  height?: number;
  width?: number;
};

export default function Divider({height = 2, width = 2}: Props) {
  return <View style={{height: height, width: width}}>{null}</View>;
}
