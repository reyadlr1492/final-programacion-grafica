import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import appTheme from '../theme/appTheme';

type Props = {
  children: React.ReactNode;
  inline?: boolean;
  handler?: any;
};
const {colors} = appTheme;
export default function Button({children, inline = false, handler}: Props) {
  return (
    <TouchableOpacity
      onPress={handler}
      style={{
        ...style.button,
        backgroundColor: inline ? 'transparent' : colors.main400,
      }}>
      {children}
    </TouchableOpacity>
  );
}

const style = StyleSheet.create({
  button: {
    paddingVertical: 18,
    paddingHorizontal: 10,
    borderRadius: 8,
  },
});
