import React from 'react';
import {StyleSheet, Text} from 'react-native';
import appTheme from '../theme/appTheme';

const {colors} = appTheme;
type Props = {
  children: React.ReactNode;
};

export default function Label({children}: Props) {
  return <Text style={style.label}>{children}</Text>;
}

const style = StyleSheet.create({
  label: {
    fontSize: 20,
    marginBottom: 8,
    color: colors.black,
  },
});
