import React from 'react';
import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import appTheme from '../theme/appTheme';
import Divider from './Divider';

const {colors} = appTheme;
export default function Loading() {
  return (
    <View style={style.loadingContainer}>
      <ActivityIndicator size={'large'} color={colors.main400} />
      <Divider height={8} />
      <Text>Cargando...</Text>
    </View>
  );
}

const style = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
